export class WatchList {

    private _id: number;
    private _userId: number;
    private _name: string;
    private _description: string;
    private _canBeDeleted: boolean;


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get userId(): number {
        return this._userId;
    }

    set userId(value: number) {
        this._userId = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get canBeDeleted(): boolean {
        return this._canBeDeleted;
    }

    set canBeDeleted(value: boolean) {
        this._canBeDeleted = value;
    }
}