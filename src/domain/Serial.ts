import Media from "./Media";
import {Movie} from "./Movie";

export class Serial extends Media {

    private _watched_series: number;

    constructor() {
        super();
    }

    static fromJson(value: object): Serial {
        let serial: Serial = new Serial();
        serial.id = value["id"];
        serial.list_id = value["list_id"];
        serial.ext_id = value["ext_id"];
        serial.media_type = value["media_type"];
        serial.watched_series = value["watched_series"];
        return serial;
    }

    static isSerial(value: object) : boolean {
        return value.hasOwnProperty('watched_series') || value.hasOwnProperty('_watched_series');
    }

    get watched_series(): number {
        return this._watched_series;
    }

    set watched_series(value: number) {
        this._watched_series = value;
    }
}