import Media from "./Media";

export class Movie extends Media {

    private _watched: boolean;

    constructor() {
        super();
    }

    static fromJson(value: object): Movie {
        let movie: Movie = new Movie();
        movie.id = value["id"];
        movie.list_id = value["list_id"];
        movie.ext_id = value["ext_id"];
        movie.media_type = value["media_type"];
        movie.watched = value["watched"];
        return movie;
    }

    static isMovie(value: object) : boolean {
        return value.hasOwnProperty('watched') || value.hasOwnProperty('_watched');
    }

    get watched(): boolean {
        return this._watched;
    }

    set watched(value: boolean) {
        this._watched = value;
    }
}