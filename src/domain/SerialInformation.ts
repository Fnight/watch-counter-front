export class SerialInformation {

    private _id: number;
    private _backdrop_path: string;
    private _original_name: string;
    private _overview: string;
    private _popularity: number;
    private _poster_path: string;
    private _name: string;
    private _status: string;
    private _first_air_date: string;
    private _episodes_count: number[];


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get backdrop_path(): string {
        return this._backdrop_path;
    }

    set backdrop_path(value: string) {
        this._backdrop_path = value;
    }

    get original_name(): string {
        return this._original_name;
    }

    set original_name(value: string) {
        this._original_name = value;
    }

    get overview(): string {
        return this._overview;
    }

    set overview(value: string) {
        this._overview = value;
    }

    get popularity(): number {
        return this._popularity;
    }

    set popularity(value: number) {
        this._popularity = value;
    }

    get poster_path(): string {
        return this._poster_path;
    }

    set poster_path(value: string) {
        this._poster_path = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get status(): string {
        return this._status;
    }

    set status(value: string) {
        this._status = value;
    }

    get first_air_date(): string {
        return this._first_air_date;
    }

    set first_air_date(value: string) {
        this._first_air_date = value;
    }

    get episodes_count(): number[] {
        return this._episodes_count;
    }

    set episodes_count(value: number[]) {
        this._episodes_count = value;
    }
}