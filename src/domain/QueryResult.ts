export class QueryResult {

    private _original_name: string;
    private _id: number;
    private _media_type: string;
    private _name: string;
    private _poster_path: string;
    private _popularity: number;
    private _original_language: string;
    private _backdrop_path: string;
    private _overview: string;
    private _first_air_date: string;

    get original_name(): string {
        return this._original_name;
    }

    set original_name(value: string) {
        this._original_name = value;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get media_type(): string {
        return this._media_type;
    }

    set media_type(value: string) {
        this._media_type = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get poster_path(): string {
        return this._poster_path;
    }

    set poster_path(value: string) {
        this._poster_path = value;
    }

    get popularity(): number {
        return this._popularity;
    }

    set popularity(value: number) {
        this._popularity = value;
    }

    get original_language(): string {
        return this._original_language;
    }

    set original_language(value: string) {
        this._original_language = value;
    }

    get backdrop_path(): string {
        return this._backdrop_path;
    }

    set backdrop_path(value: string) {
        this._backdrop_path = value;
    }

    get overview(): string {
        return this._overview;
    }

    set overview(value: string) {
        this._overview = value;
    }

    get first_air_date(): string {
        return this._first_air_date;
    }

    set first_air_date(value: string) {
        this._first_air_date = value;
    }
}