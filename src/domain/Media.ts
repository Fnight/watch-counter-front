export default class Media {
    static MOVIE_TYPE: string = "movie";
    static SERIAL_TYPE: string = "tv";

    private _id: number;
    private _ext_id: number;
    private _list_id: number;
    private _media_type: string;

    constructor() {
    }

    static fromJson(value: object): Media {
        let media: Media = new Media();
        media.id = value["id"];
        media.ext_id = value["ext_id"];
        media.list_id = value["list_id"];
        media.media_type = value["media_type"];
        return media;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get ext_id(): number {
        return this._ext_id;
    }

    set ext_id(value: number) {
        this._ext_id = value;
    }

    get list_id(): number {
        return this._list_id;
    }

    set list_id(value: number) {
        this._list_id = value;
    }

    get media_type(): string {
        return this._media_type;
    }

    set media_type(value: string) {
        this._media_type = value;
    }
}