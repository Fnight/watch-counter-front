export class InformationUtils {

    static getCurrentSeason(current: number, seasons: number[]): number {
        let currentSeason: number = 1;
        let currentEpisodes: number = 0;
        seasons.forEach((value: number) => {
            if (current < currentEpisodes) {
                currentEpisodes += value;
                currentSeason++;
            }
        });
        return currentSeason;
    }

    static getEpisodeInSeason(current: number, seasons: number[]): number {
        let currentEpisodes: number = current;
        seasons.forEach((value: number) => {
            if (currentEpisodes - value > 0) {
                currentEpisodes -= value;
            }
        });
        return currentEpisodes;
    }
}