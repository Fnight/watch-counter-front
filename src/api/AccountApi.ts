import {BaseApi} from "./BaseApi";

export default class AccountApi extends BaseApi{

    static checkAuth(): Promise<Response> {
        let url: string = this.API_URL + "/user/account";
        return fetch(url, {
            method: 'GET',
            credentials: 'include',
        });
    }

    static login(login: string, password: string): Promise<Response> {
        let url: string = this.API_URL + "/user/login";
        let formData = new FormData();
        formData.append("username", login);
        formData.append("password", password);
        return fetch(url, {
            method: 'POST',
            credentials: 'include',
            body: formData
        });
    }

}