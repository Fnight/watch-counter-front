import {BaseApi} from "./BaseApi";

export default class ListsApi extends BaseApi {

    static myLists(): Promise<Response> {
        let url: string = this.API_URL + "/list/my";
        return fetch(url, {
            method: 'GET',
            credentials: 'include',
        });
    }

    static mediasFromList(id: number): Promise<Response> {
        let url: string = this.API_URL + "/list/" + id + "/medias";
        return fetch(url, {
            method: 'GET',
            credentials: 'include',
        });
    }

}