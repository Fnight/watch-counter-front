import {BaseApi} from "./BaseApi";

export default class MediaApi extends BaseApi {

    static getMedia(id: number, type: string): Promise<Response> {
        let url: string = this.API_URL + "/media/" + id + "?mediaType=" + type;
        return fetch(url, {
            method: 'GET',
            credentials: 'include',
        });
    }

    static addMedia(extId: number, type: string, listId: number): Promise<Response> {
        let url: string = this.API_URL + "/list/" + listId + "/media?mediaType=" + type + "&extId=" + extId;
        return fetch(url, {
            method: 'POST',
            credentials: 'include'
        });
    }

}