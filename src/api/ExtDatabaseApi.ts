import {BaseApi} from "./BaseApi";

export default class ExtDatabaseApi extends BaseApi {

    static getMovieInforation(id: number): Promise<Response> {
        let url: string = this.API_URL + "/database/movie/" + id;
        return fetch(url, {
            method: 'GET',
            credentials: 'include'
        });
    }

    static getSerialInforation(id: number): Promise<Response> {
        let url: string = this.API_URL + "/database/serial/" + id;
        return fetch(url, {
            method: 'GET',
            credentials: 'include'
        });
    }

    static search(search: string): Promise<Response> {
        let url: string = this.API_URL + "/database/query?query=" + encodeURI(search);
        return fetch(url, {
            method: 'GET',
            credentials: 'include'
        });
    }

}