import * as React from "react";
import {
    Panel,
    Label,
    Button,
    ButtonGroup,
    Image,
    DropdownButton,
    MenuItem,
    Well,
    Checkbox
} from "react-bootstrap";
import Media from "../domain/Media";
import {MovieInformation} from "../domain/MovieInformation";
import {SerialInformation} from "../domain/SerialInformation";
import ExtDatabaseApi from "../api/ExtDatabaseApi";
import {Serial} from "../domain/Serial";
import {Movie} from "../domain/Movie";
import "../assets/scss/MediaComponent.scss"
import {InformationUtils} from "../Utils/InformationUtils";

export interface TableMediaComponentProps {
    media: Serial | Movie;
    position: number;
    selectionMode: boolean;
}

export interface TableMediaComponentStates {
    info: MovieInformation | SerialInformation;
}

export default class TableMediaComponent extends React.Component<TableMediaComponentProps, TableMediaComponentStates> {
    constructor(props, context) {
        super(props, context);

        // this.hideAuth = this.hideAuth.bind(this);

        this.state = {
            info: null,
        };
    }

    componentDidMount(): void {
        let media: Media = this.props.media;
        if (Movie.isMovie(media)) {
            ExtDatabaseApi.getMovieInforation(media.ext_id).then((response: Response) => {
                response.json().then((value: any) => {
                    this.setState({info: value});
                });
            });
        } else {
            ExtDatabaseApi.getSerialInforation(media.ext_id).then((response: Response) => {
                response.json().then((value: any) => {
                    this.setState({info: value});
                });
            });
        }
    }

    statusPart(): JSX.Element[] {
        let resultStatus: string = "";

        const media: Serial = this.props.media as Serial;
        if (media.watched_series != null && this.state.info) {
            const info: SerialInformation = this.state.info as SerialInformation;
            const watchedSeries: number = media.watched_series;
            const currentSeason: number = InformationUtils.getCurrentSeason(watchedSeries, info.episodes_count);
            resultStatus = `${currentSeason}/${info.episodes_count.length} сезон - ${InformationUtils.getEpisodeInSeason(watchedSeries,
                info.episodes_count)}/${info.episodes_count[currentSeason - 1]}`
        }

        return (
            [
                <td>
                    {resultStatus}
                </td>
                ,
                <td>
                    {this.props.media instanceof Movie
                        ? this.props.media.watched
                            ? <Checkbox inline checked>Просмотрено</Checkbox>
                            : <Checkbox inline>Просмотрено</Checkbox>
                        : <ButtonGroup>
                            <Button bsSize="xs" bsStyle="warning">-</Button>
                            <Button bsSize="xs" bsStyle="success">+</Button>
                        </ButtonGroup>
                    }
                </td>
            ]
        );
    }

    render() {
        let media: Serial | Movie = this.props.media;
        let title: string = "";
        let stringType: string = "";
        if (this.state.info) {
            if (this.state.info.name) {
                title = this.state.info.name;
            } else {
                title = this.state.info.original_name;
            }
        }
        if (media) {
            if (media instanceof Movie) {
                stringType = "Фильм";
            } else {
                stringType = "Сериал";
            }
        }
        return (
            <tr>
                <td>{this.props.position + 1}</td>
                <td>
                    {this.state.info && this.state.info.first_air_date
                        ? <Label bsStyle="primary">{
                            new Date(this.state.info.first_air_date).getFullYear()
                        }</Label>
                        : <div/>
                    }
                    {" "}{title}
                </td>
                <td>{stringType}</td>
                <td>Оценка</td>
                {this.statusPart()}
                {
                    this.props.selectionMode
                        ? <Checkbox inline/>
                        : <div/>
                }
            </tr>
        );
    }
}
