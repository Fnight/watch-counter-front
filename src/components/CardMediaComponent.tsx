import * as React from "react";
import {
    Panel,
    Label,
    Button,
    ButtonGroup,
    Image,
    Media as BMedia,
    DropdownButton,
    MenuItem,
    Well
} from "react-bootstrap";
import Media from "../domain/Media";
import {MovieInformation} from "../domain/MovieInformation";
import {SerialInformation} from "../domain/SerialInformation";
import ExtDatabaseApi from "../api/ExtDatabaseApi";
import {Serial} from "../domain/Serial";
import {Movie} from "../domain/Movie";
import "../assets/scss/MediaComponent.scss"

export interface CardMediaComponentProps {
    media: Serial | Movie;
}

export interface CardMediaComponentStates {
    info: MovieInformation | SerialInformation;
}

export default class CardMediaComponent extends React.Component<CardMediaComponentProps, CardMediaComponentStates> {
    constructor(props, context) {
        super(props, context);

        // this.hideAuth = this.hideAuth.bind(this);

        this.state = {
            info: null,
        };
    }

    componentDidMount(): void {
        let media: Media = this.props.media;
        if (media.media_type === Media.MOVIE_TYPE) {
            ExtDatabaseApi.getMovieInforation(media.ext_id).then((response: Response) => {
                response.json().then((value: any) => {
                    this.setState({info: value});
                });
            });
        } else {
            ExtDatabaseApi.getSerialInforation(media.ext_id).then((response: Response) => {
                response.json().then((value: any) => {
                    this.setState({info: value});
                });
            });
        }
    }

    render() {
        let media: Serial | Movie = this.props.media;
        let title: string = "";
        if (this.state.info) {
            if (this.state.info.name) {
                title = this.state.info.name;
            } else {
                title = this.state.info.original_name;
            }
        }
        let status: string;
        if (media) {
            if (media instanceof Movie) {
                let movie: Movie = media as Movie;
                status = movie.watched ? "Просмотрено" : "Не просмотрено";
            } else {
                let serial: Serial = media as Serial;
                status = String(serial.watched_series);
            }
        }
        return (
            <div className="media">
                <Well>
                    {
                        this.state.info && this.state.info.poster_path
                            ? <Image src={"https://image.tmdb.org/t/p/h100" + this.state.info.poster_path}/>
                            : <div/>
                    }
                    {title}
                    <Label>{this.props.media instanceof Movie ? 'Фильм' : 'Сериал'}</Label>
                    <br/>
                    <Label>{status}</Label>
                    <br/>
                    <DropdownButton
                        className="dropdown-raiting"
                        title="Оцнека"
                    >
                        <MenuItem eventKey="1">Action</MenuItem>
                        <MenuItem eventKey="2">Another action</MenuItem>
                        <MenuItem eventKey="3" active>
                            Active Item
                        </MenuItem>
                        <MenuItem divider/>
                        <MenuItem eventKey="4">Separated link</MenuItem>
                    </DropdownButton>
                    <ButtonGroup>
                        {
                            this.props.media instanceof Movie
                                ? <Button>Просмотрено</Button>
                                : [
                                    <Button>+</Button>,
                                    <Button> -</Button>,
                                    <DropdownButton title="s" className="dropdown-raiting" id="bg-nested-dropdown">
                                        <MenuItem eventKey="1">Dropdown link</MenuItem>
                                        <MenuItem eventKey="2">Dropdown link</MenuItem>
                                    </DropdownButton>
                                ]
                        }
                    </ButtonGroup>
                </Well>
            </div>
        );
    }
}
