import * as React from "react";
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from "react-bootstrap";
// import "./../assets/scss/Header.scss";

export interface HeaderProps {
}

export default class Header extends React.Component<HeaderProps, undefined> {
    render() {
        return (
            <div className="header">
                <Navbar>
                    <Navbar.Header>
                        <Navbar.Brand>
                            Watch Counter
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav>
                            {/*<NavItem href="#">*/}
                            {/*Link*/}
                            {/*</NavItem>*/}
                        </Nav>
                        <Nav pullRight>
                            <NavDropdown eventKey={3} title="Вид" id="basic-nav-dropdown">
                                <MenuItem eventKey={3.1}>Карточки</MenuItem>
                                <MenuItem eventKey={3.2}>Таблица</MenuItem>
                            </NavDropdown>
                            <NavItem href="https://vk.com/fnight">
                                FNight
                            </NavItem>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
}
