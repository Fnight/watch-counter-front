import * as React from "react";
import Header from "./Header";
import Auth from "./auth/Auth";
import WatchLists from "./WatchLists";
import AccountApi from "../api/AccountApi";
import {WatchList} from "../domain/WatchList";
import ListsApi from "../api/ListsApi";

export interface AppProps {
}

export interface AppState {
    isAuth: boolean;
    watchLists: WatchList[];
}

export default class App extends React.Component<AppProps, AppState> {
    constructor(props, context) {
        super(props, context);

        this.onComplete = this.onComplete.bind(this);

        this.state = {
            isAuth: true,
            watchLists: null
        };
    }


    componentDidMount(): void {
        AccountApi.checkAuth().then((response: Response) => {
            if (response.status == 200) {
                this.onComplete();
            } else {
                this.setState({isAuth: false});
            }
        });
    }

    onComplete(): void {
        ListsApi.myLists().then((response: Response) => {
            response.json().then((value: WatchList[]) => {
                this.setState({watchLists: value, isAuth: true});
            });
        });
    }

    render() {
        return (
            <div className="app">
                <Header/>
                {this.state.isAuth
                    ? <WatchLists watchLists={this.state.watchLists}/>
                    : <Auth onComplete={this.onComplete}/>
                }
            </div>
        );
    }
}
