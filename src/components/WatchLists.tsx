import * as React from "react";
import {Tabs, Tab, Row, Col, Nav, NavItem} from "react-bootstrap";
import {WatchList} from "../domain/WatchList";
import WatchListComponent from "./WatchListComponent";
import "../assets/scss/WatchLists.scss";

export interface WatchListsProps {
    watchLists: WatchList[];
}

export interface WatchListsState {
}

export default class WatchLists extends React.Component<WatchListsProps, WatchListsState> {
    constructor(props, context) {
        super(props, context);

        // this.hideAuth = this.hideAuth.bind(this);

        this.state = {
            watchLists: []
        };
    }

    componentDidMount(): void {

    }

    render() {
        return (
            <div className="watch-lists">
                <Tab.Container id="watch-lists">
                    <Row className="clearfix">
                        <Col sm={2}>
                            <Nav bsStyle="pills" stacked>
                                {
                                    this.props.watchLists && this.props.watchLists.map(
                                        (list: WatchList) => <NavItem eventKey={list.id}>{list.name}</NavItem>
                                    )
                                }
                            </Nav>
                        </Col>
                        <Col sm={10}>
                            <Tab.Content animation>
                                {
                                    this.props.watchLists && this.props.watchLists.map(
                                        (list: WatchList) =>
                                            <Tab.Pane eventKey={list.id}>
                                                <WatchListComponent watchList={list} isTable={true}/>
                                            </Tab.Pane>
                                    )
                                }
                            </Tab.Content>
                        </Col>
                    </Row>
                </Tab.Container>
            </div>
        );
    }
}
