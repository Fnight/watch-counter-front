import * as React from "react";
import {Modal, FormGroup, ControlLabel, FormControl, Button, Alert} from "react-bootstrap";
import AccountApi from "../../api/AccountApi";

export interface LoginProps {
    hide(): void;
}

export interface LoginState {
    login: string;
    password: string;
    lastError: string;
}

export default class Login extends React.Component<LoginProps, LoginState> {
    constructor(props, context) {
        super(props, context);

        this.state = {
            login: "",
            password: "",
            lastError: ""
        };

        this.login = this.login.bind(this);
        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
    }

    showError(error: string): void {
        this.setState({lastError: error});
    }

    login(): void {
        AccountApi.login(this.state.login, this.state.password)
            .then((response: Response) => {
                console.log(response.status);
                if (response.status == 200) {
                    this.props.hide();
                } else if (response.status == 401) {
                    this.showError("Неверный логин или пароль");
                }
            });
    }

    handleChangeLogin(e): void {
        this.setState({login: e.target.value});
    }

    handleChangePassword(e): void {
        this.setState({password: e.target.value});
    }

    render() {
        return (
            <div className="login">
                <Modal.Body>
                    {this.state.lastError != "" ?
                        <Alert bsStyle="danger">
                            {this.state.lastError}
                        </Alert> : <div/>
                    }

                    <FormGroup controlId="loginLogin">
                        <ControlLabel>Логин</ControlLabel>
                        <FormControl
                            placeholder="Введите логин"
                            value={this.state.login}
                            onChange={this.handleChangeLogin}/>
                    </FormGroup>
                    <FormGroup controlId="loginPassword">
                        <ControlLabel>Пароль</ControlLabel>
                        <FormControl
                            type="password"
                            placeholder="Введите пароль"
                            value={this.state.password}
                            onChange={this.handleChangePassword}/>
                    </FormGroup>

                </Modal.Body>

                <Modal.Footer>
                    <Button bsStyle="primary" onClick={this.login}>
                        Логин
                    </Button>
                </Modal.Footer>
            </div>
        );
    }
}
