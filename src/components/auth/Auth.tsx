import * as React from "react";
import {Modal, Tabs, Tab, Button, FormGroup, ControlLabel, FormControl} from "react-bootstrap";
import Registration from "./Registration";
import Login from "./Login";
import AccountApi from "../../api/AccountApi";

export interface AuthProps {
    onComplete(): void;
}

export interface AuthState {
    show: boolean;
}

export default class Auth extends React.Component<AuthProps, AuthState> {
    constructor(props, context) {
        super(props, context);

        this.hideAuth = this.hideAuth.bind(this);

        this.state = {
            show: true
        };
    }

    hideAuth(): void {
        this.setState({show: false});
        this.props.onComplete();
    }

    render() {
        return (
            <div className="auth">
                <Modal show={this.state.show}>
                    <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
                        <Tab eventKey={1} title="Авторизация">
                            <Login hide={this.hideAuth}/>
                        </Tab>
                        <Tab eventKey={2} title="Регистрация">
                            <Registration/>
                        </Tab>
                    </Tabs>
                </Modal>
            </div>
        );
    }
}
