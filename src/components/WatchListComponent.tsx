import * as React from "react";
import {Panel, Grid, Row, Col, DropdownButton, MenuItem, Table} from "react-bootstrap";
import ListsApi from "../api/ListsApi";
import {WatchList} from "../domain/WatchList";
import CardMediaComponent from "./CardMediaComponent";
import {Movie} from "../domain/Movie";
import {Serial} from "../domain/Serial";
import TableMediaComponent from "./TableMediaComponent";
import AddMediaModal from "./AddMediaModal";
import MediaApi from "../api/MediaApi";

export interface WatchListProps {
    watchList: WatchList;
    isTable: boolean;
}

export interface WatchListState {
    medias: Array<Serial | Movie>;
    showAddModal: boolean;
    selectionMode: boolean;
}

export default class WatchListComponent extends React.Component<WatchListProps, WatchListState> {
    constructor(props, context) {
        super(props, context);

        this.showAddModal = this.showAddModal.bind(this);
        this.closeAddModal = this.closeAddModal.bind(this);
        this.addNewMedia = this.addNewMedia.bind(this);

        this.state = {
            medias: [],
            showAddModal: false,
            selectionMode: false
        };
    }

    cardView(): JSX.Element {
        return (
            <Grid>
                <Row>
                    {
                        this.state.medias.map((media: Movie | Serial) =>
                            <Col xs={6} md={4}>
                                <CardMediaComponent media={media}/>
                            </Col>)
                    }
                </Row>
            </Grid>
        );
    }

    tableView(): JSX.Element {
        return (
            <Table responsive>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Тип</th>
                    <th>Оценка</th>
                    <th>Прогресс</th>
                    {this.state.selectionMode
                        ? <th><a onClick={() => this.setState({selectionMode: false})}>Отменить</a></th>
                        : <th><a onClick={() => this.setState({selectionMode: true})}>Выбрать</a></th>
                    }
                </tr>
                </thead>
                <tbody>
                {
                    this.state.medias.map((media: Movie | Serial, index: number) =>
                        <TableMediaComponent
                            media={media}
                            position={index}
                            selectionMode={this.state.selectionMode}
                        />
                    )
                }
                <tr>
                    <th colSpan={5}>
                        <a onClick={this.showAddModal}>Добавить</a>
                    </th>
                        {
                            this.state.selectionMode
                                ? <th><a>Удалить</a></th>
                                : <th/>
                        }
                </tr>
                </tbody>
            </Table>
        );
    }

    showAddModal(): void {
        this.setState({showAddModal: true});
    }

    closeAddModal(): void {
        this.setState({showAddModal: false});
    }

    addNewMedia(choose: string): void {
        const chooseArray: string[] = choose.split("/");
        MediaApi.addMedia(parseInt(chooseArray[0]), chooseArray[1],
            this.props.watchList.id).then((response: Response) => {
            response.json().then((value: Serial | Movie) => {
                let medias: Array<Serial | Movie> = this.state.medias;
                if (Movie.isMovie(value)) {
                    medias.push(Movie.fromJson(value));
                } else {
                    medias.push(Serial.fromJson(value));
                }
                this.setState({medias: medias, showAddModal: false});
            });
        });
    }

    componentDidMount(): void {
        let list: WatchList = this.props.watchList;
        ListsApi.mediasFromList(list.id).then((response: Response) => {
            response.json().then((value: Array<Serial | Movie>) => {
                let medias: Array<Serial | Movie> = value.map((obj: object) =>
                    Movie.isMovie(obj) ? Movie.fromJson(obj) : Serial.fromJson(obj));
                this.setState({medias: medias});
            });
        });
    }

    render() {
        return (
            <div className="watch-list">
                {this.props.isTable ? this.tableView() : this.cardView()}
                <AddMediaModal
                    addMedia={this.addNewMedia}
                    show={this.state.showAddModal}
                    onHide={this.closeAddModal}/>
            </div>
        );
    }
}
