import * as React from "react";
import {Modal, Tabs, Tab, Button, FormGroup, ControlLabel, FormControl, FieldGroup, Radio, Label} from "react-bootstrap";
import {QueryResult} from "../domain/QueryResult";
import ExtDatabaseApi from "../api/ExtDatabaseApi";
import Media from "../domain/Media";
import "../assets/scss/AddMediaModal.scss"

export interface AddMediaModalProps {
    show: boolean;
    addMedia(choose: string): void;
    onHide(): void;
}

export interface AddMediaModalState {
    search: string;
    result: QueryResult[];
    choose: string;
}

export default class AddMediaModal extends React.Component<AddMediaModalProps, AddMediaModalState> {
    constructor(props, context) {
        super(props, context);

        this.handleChangeSearch = this.handleChangeSearch.bind(this);
        this.handleChangeChoose = this.handleChangeChoose.bind(this);
        this.addNewMedia = this.addNewMedia.bind(this);

        this.state = {
            search: "",
            result: [],
            choose: ""
        };
    }

    handleChangeSearch(e): void {
        this.setState({search: e.target.value, choose: null}, () => {
            if (this.state.search != "") {
                ExtDatabaseApi.search(this.state.search).then(value => {
                    value.json().then(json => this.setState({result: json}));
                });
            }
        });
    }

    handleChangeChoose(e): void {
        this.setState({choose: e.target.value});
    }

    hide(): void {
    }

    addNewMedia(): void {
        this.props.addMedia(this.state.choose);
    }

    render() {
        return (
            <div className="add-media-modal">
                <Modal show={this.props.show} onHide={this.props.onHide}>
                    <Modal.Header closeButton>
                        <Modal.Title>Добавить новое медиа</Modal.Title>
                    </Modal.Header>
                    <FormGroup controlId="loginLogin" className="search-field">
                        <ControlLabel>Поиск</ControlLabel>
                        <FormControl
                            placeholder="Введите название..."
                            value={this.state.search}
                            onChange={this.handleChangeSearch}/>
                        {this.state.result.map((result: QueryResult) =>
                            <Radio name="queryResult" value={`${result.id}/${result.media_type}`} onChange={this.handleChangeChoose}>
                                {
                                    result.first_air_date
                                        ? <Label>{new Date(result.first_air_date).getFullYear()}</Label>
                                        : <div/>
                                }
                                {" "}{result.name}{" "}
                                <Label>{result.media_type == Media.MOVIE_TYPE ? "Фильм" : "Сериал"}</Label>
                            </Radio>
                        )}
                    </FormGroup>
                    <Modal.Footer>
                        <Button onClick={this.props.onHide}>Отмена</Button>
                        {
                            this.state.choose
                                ? <Button onClick={this.addNewMedia} bsStyle="primary">Добавить</Button>
                                : <Button bsStyle="primary" disabled>Добавить</Button>
                        }
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
